﻿CREATE TABLE [dbo].[W6Audit_HISTORY] (
    [RecordIdentifier]          INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Name]                      VARCHAR (64)   NULL,
    [TaskCallID]                VARCHAR (64)   NULL,
    [AuditTimeStamp]            DATETIME       NULL,
    [ObjectType]                INT            NULL,
    [AuditAction]               INT            NULL,
    [TaskStatus]                VARCHAR (64)   NULL,
    [Region]                    VARCHAR (64)   NULL,
    [District]                  VARCHAR (64)   NULL,
    [StartTime]                 DATETIME       NULL,
    [FinishTime]                DATETIME       NULL,
    [Engineers]                 VARCHAR (640)  NULL,
    [TaskKey]                   INT            NULL,
    [LocationVerificationError] VARCHAR (128)  NULL,
    [LastNotificationSent]      VARCHAR (64)   NULL,
    [NotifyLongTerm]            INT            NULL,
    [NotifyMidTerm]             INT            NULL,
    [NotifyShortTerm]           INT            NULL,
    [NotifySMS]                 INT            NULL,
    [NotifyEmail]               INT            NULL,
    [NotifyIVR]                 INT            NULL,
    [AppointmentStart]          DATETIME       NULL,
    [DispatcherComment]         VARCHAR (4000) NULL,
    [Comment]                   VARCHAR (1000) NULL,
    [ReceiverLine]              VARCHAR (15)   NULL,
    [AppointmentFinish]         DATETIME       NULL,
    [RuleViolations]            VARCHAR (1000) NULL,
    [JeopardyState]             VARCHAR (64)   NULL,
    [IsRuleViolating]           INT            NULL,
    [RuleViolationText]         VARCHAR (4000) NULL,
    [ChangeDescription]         VARCHAR (64)   NULL,
    [TaskNumber]                INT            NULL,
    [TaskDuration]              INT            NULL,
    [FSPro_User]                NCHAR (100)    NULL,
    PRIMARY KEY CLUSTERED ([RecordIdentifier] ASC)
);

